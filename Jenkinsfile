pipeline {
    agent any
    stages {
        stage('increment version') {
            steps {
                script {
                    // enter app directory, because that's where package.json is located
                    dir("app") {
                        // update application version in the package.json file with one of these release types: patch, minor or major
                        // this will commit the version update
                        sh "npm version minor"

                        // read the updated version from the package.json file
                        def packageJson = readJSON file: 'package.json'
                        def version = packageJson.version

                        // set the new version as part of IMAGE_NAME
                        env.IMAGE_NAME = "$version-$BUILD_NUMBER"
                    }

                    // alternative solution without Pipeline Utility Steps plugin:
                    // def version = sh (returnStdout: true, script: "grep 'version' package.json | cut -d '\"' -f4 | tr '\\n' '\\0'")
                    // env.IMAGE_NAME = "$version-$BUILD_NUMBER"
                }
            }
        }
        stage('Run tests') {
            steps {
               script {
                    // enter app directory, because that's where package.json and tests are located
                    dir("app") {
                        // install all dependencies needed for running tests
                        sh "npm install"
                        sh "npm run test"
                    }
               }
            }
        }
        stage('Build and Push docker image') {
            steps {
                withCredentials([usernamePassword(credentialsId: 'docker-hub', usernameVariable: 'USER', passwordVariable: 'PASS')]){
                    sh "docker build -t coolkartal/demo-app:${IMAGE_NAME} ."
                    sh 'echo $PASS | docker login -u $USER --password-stdin'
                    sh "docker push coolkartal/demo-app:${IMAGE_NAME}"
                }
            }
        }
        stage('Edit Docker Compose File') {
            steps {
                script {
                    // Replace '${IMAGE_NAME}' with the actual image name in docker-compose.yaml
                    sh "sed -i 's/\${IMAGE_NAME}/coolkartal\\/demo-app:${IMAGE_NAME}/' docker-compose.yml"
                }
            }
        }
        stage('deploy'){
            steps {
                script {
                echo 'deployingg docker image to EC2...'
                def dockerComposeCmd = "docker-compose up -d"
                sshagent(['aws-cre']) {
                    sh "scp -o StrictHostKeyChecking=no docker-compose.yml ec2-user@3.68.223.34:/home/ec2-user/"
                    sh "ssh -o StrictHostKeyChecking=no ec2-user@3.68.223.34 ${dockerComposeCmd}"
                    }
                }
            }
        }
        stage('Revert Docker Compose File for Git') {
            steps {
                script {
                    // Revert '${IMAGE_NAME}' to placeholder in docker-compose-template.yml for Git commit
                    sh "sed -i 's/coolkartal\\/demo-app:${IMAGE_NAME}/\${IMAGE_NAME}/' docker-compose.yml"
                }
            }
        }
        stage('commit version update') {
            steps {
                script {
                    withCredentials([usernamePassword(credentialsId: 'git-lab', usernameVariable: 'USER', passwordVariable: 'PASS')]) {
                        // git config here for the first time run
                        sh 'git config --global user.email "c4n.kartal@jenkins.com"'
                        sh 'git config --global user.name "jenkins"'
                        sh 'git remote set-url origin https://$USER:$PASS@gitlab.com/devops7443513/9_aws-exercises.git'
                        sh 'git add .'
                        sh 'git commit -m "ci: version bump"'
                        sh 'git push origin HEAD:main'
                    }
                }
            }
        }
    }
}
